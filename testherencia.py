from herencia import Persona
from herencia import Paciente
from herencia import Médico
import unittest

class TestHerencia(unittest.TestCase):

    def test_historial_clinico(self): 
        paciente=Paciente('María', 'Aparisi Anuarbe', '02/10/2004', '53992236N', 'Enferma de riñón')
        historial=paciente.ver_historial_clinico()
        self.assertEqual(historial, "Historial clínico: Enferma de riñón")
    
    def test_consultar_agenda(self): 
        medico=Médico ('Roberto', 'Romera Calvo', '26/04/1973', '98765432H', 'Dermatología', '29/02/2023 a las 14:30')
        agenda=medico.consultar_agenda()
        self.assertEqual(agenda, "Hay una cita programada el día 29/02/2023 a las 14:30 en la especialidad de Dermatología")


if __name__ == "__main__":
    unittest.main()