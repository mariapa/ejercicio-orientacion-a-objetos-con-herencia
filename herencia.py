class Persona:
    def __init__(self, nombre, apellidos, nacimiento, DNI): 
        self.nombre=nombre
        self.apellidos = apellidos
        self.nacimiento = nacimiento
        self.DNI = DNI
    
    def setNombre(self, nombre):
        self.nombre=nombre
    
    def getNombre(self):
        return self.nombre

    def setApellidos(self, apellidos):
        self.apellidos=apellidos
    
    def getApellidos(self):
        return self.apellidos
    
    def setNacimiento(self, nacimiento):
        self.nacimiento=nacimiento
    
    def getNacimiento(self):
        return self.nacimiento
    
    def setDNI(self, DNI):
        self.DNI=DNI
    
    def getDNI(self):
        return self.DNI

    def __str__(self):
        return "La persona se llama {name} {surname}, nació el {date} y tiene DNI: {Dni}".format(name=self.nombre, surname=self.apellidos, date=self.nacimiento, Dni=self.DNI)

class Paciente (Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, hist):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self.historial_clinico=hist

    def ver_historial_clinico(self):
        return 'Historial clínico: {historial}'.format (historial=self.historial_clinico)

class Médico (Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, espec, cit):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self.especialidad=espec
        self.cita=cit
    
    def consultar_agenda(self):
        return 'Hay una cita programada el día {cit} en la especialidad de {espec}'.format(cit=self.cita, espec=self.especialidad)


persona=Persona('Tania', 'Varas Medina', '01/09/2004', '12345678B')
paciente=Paciente('María', 'Aparisi Anuarbe', '02/10/2004', '53992236N', 'Enferma de riñón')
medico=Médico ('Roberto', 'Romera Calvo', '26/04/1973', '98765432H', 'Dermatología', '29/02/2023 a las 14:30')

gente=[persona, paciente, medico]
for people in gente:
    print (people)
    if people==paciente:
        print(people.ver_historial_clinico())
    elif people==medico:
        print(people.consultar_agenda())